package de.spigotplugins.statsmanager;

import de.spigotplugins.statsmanager.commands.StatsCommand;
import de.spigotplugins.statsmanager.listener.PlayerDeathListener;
import de.spigotplugins.statsmanager.listener.PlayerQuitListener;
import de.spigotplugins.statsmanager.manager.PlayerStatistic;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

/**
 * Created by Pascal Falk on 03.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public final class StatsManager extends JavaPlugin {
	private static final String PREFIX = "§7[§eStatsManager§7] ";
	private final Map<UUID, PlayerStatistic> statistics = new HashMap<>();
	private YamlConfiguration yamlConfiguration = new YamlConfiguration();
	private File file;

	@Override
	public void onEnable() {
		file = new File(getDataFolder(), "playerstats.yml");

		if (file.exists()) {
			try {
				yamlConfiguration.load(file);
			} catch (final IOException | InvalidConfigurationException e) {
				getLogger().log(Level.SEVERE, "Error while loading the statistics file", e);
			}
		}

		final PluginManager pluginManager = Bukkit.getPluginManager();
		pluginManager.registerEvents(new PlayerDeathListener(this), this);
		pluginManager.registerEvents(new PlayerQuitListener(this), this);

		getCommand("stats").setExecutor(new StatsCommand(this));
	}

	@Override
	public void onDisable() {
		statistics.forEach((uuid, statistic) -> {
			final String prefix = statistic.getUniqueID().toString() + ".";
			yamlConfiguration.set(prefix + "kills", statistic.getKillCount());
			yamlConfiguration.set(prefix + "deaths", statistic.getDeathCount());
		});

		try {
			yamlConfiguration.save(file);
		} catch (IOException e) {
			getLogger().log(Level.SEVERE, "Error while saving the statistics file", e);
		}
	}

	private PlayerStatistic loadStatistic(final UUID uuid) {
		if (statistics.containsKey(uuid)) {
			statistics.remove(uuid);
		}

		if (yamlConfiguration.isConfigurationSection(uuid.toString())) {
			System.out.println("Is section");
			final String prefix = uuid.toString() + ".";
			int kills = 0;
			int deaths = 0;

			if (yamlConfiguration.contains(prefix + "kills")) {
				System.out.println("Add kills");
				kills = yamlConfiguration.getInt(prefix + "kills");
			}

			if (yamlConfiguration.contains(prefix + "deaths")) {
				System.out.println("add deaths");
				deaths = yamlConfiguration.getInt(prefix + "deaths");
			}

			return new PlayerStatistic(uuid, kills, deaths);
		}
		System.out.println("no section");
		return new PlayerStatistic(uuid);
	}

	public void unloadStatistic(final UUID uuid) {
		System.out.println("uuid = [" + uuid + "]");
		System.out.println("statistics = " + statistics);
		if (!statistics.containsKey(uuid)) {
			return;
		}
		System.out.println("saving");
		saveStatistic(statistics.remove(uuid));
	}

	private void saveStatistic(final PlayerStatistic statistic) {
		final String prefix = statistic.getUniqueID().toString() + ".";
		yamlConfiguration.set(prefix + "kills", statistic.getKillCount());
		yamlConfiguration.set(prefix + "deaths", statistic.getDeathCount());
		try {
			yamlConfiguration.save(file);
		} catch (IOException e) {
			getLogger().log(Level.SEVERE, "Error while saving the statistics file", e);
		}
	}

	public int incrementKillCount(final UUID uuid) {
		return getStatistic(uuid).incrementKillCount();
	}

	public int incrementDeathCount(final UUID uuid) {
		return getStatistic(uuid).incrementDeathCount();
	}

	public PlayerStatistic getStatistic(final UUID uuid) {
		System.out.println("Getting");
		System.out.println("statistics = " + statistics.get(uuid));
		return statistics.computeIfAbsent(uuid, this::loadStatistic);
	}

	public Map<UUID, PlayerStatistic> getPlayerManagerMap() {
		return statistics;
	}

	public File getFile() {
		return file;
	}

	public static String getPrefix() {
		return PREFIX;
	}

}
