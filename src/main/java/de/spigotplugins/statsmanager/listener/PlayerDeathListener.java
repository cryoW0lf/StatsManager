package de.spigotplugins.statsmanager.listener;

import de.spigotplugins.statsmanager.StatsManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * Created by Pascal Falk on 03.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public final class PlayerDeathListener implements Listener {
	private final StatsManager statsManager;

	public PlayerDeathListener(final StatsManager statsManager) {
		this.statsManager = statsManager;
	}

	@EventHandler
	public void onDeath(final PlayerDeathEvent event) {
		statsManager.incrementDeathCount(event.getEntity().getUniqueId());

		if (event.getEntity().getKiller() == null || !(event.getEntity().getKiller() instanceof Player)) {
			return;
		}

		statsManager.incrementKillCount(event.getEntity().getKiller().getUniqueId());
	}
}
