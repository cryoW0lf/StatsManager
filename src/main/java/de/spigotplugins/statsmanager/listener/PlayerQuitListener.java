package de.spigotplugins.statsmanager.listener;

import de.spigotplugins.statsmanager.StatsManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Pascal Falk on 03.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public final class PlayerQuitListener implements Listener {
	private final StatsManager statsManager;

	public PlayerQuitListener(final StatsManager statsManager) {
		this.statsManager = statsManager;
	}

	@EventHandler
	public void onQuit(final PlayerQuitEvent event) {
		statsManager.unloadStatistic(event.getPlayer().getUniqueId());
	}
}
